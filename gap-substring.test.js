const gapSubstring = require('./gap-substring');

describe(gapSubstring, function() {
  test('jokers, smookes => okes', function() {
    expect(gapSubstring('jokers', 'smookes')).toBe('okes');
  })

  // Failing Currently
  test('abbacdks, bakds => baks', function() {
    expect(gapSubstring('abbacdks', 'bakds')).toBe('baks');
  })

  // This is the reverse of above, and passes.
  test('bakds, abbacdks => baks', function() {
    expect(gapSubstring('bakds', 'abbacdks')).toBe('baks');
  })

  test('atcba, gattaca, => atca', function() {
    expect(gapSubstring('atcba', 'gattaca')).toBe('atca');
  })
})