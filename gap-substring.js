module.exports = function longestSubstring(a, b) {
  let attempts = [];
  let start = 0;

  while (start < a.length) {
    let indA = start;
    let indB = 0;
    attempts.push('');

    while (indA < a.length && indB < b.length) {
      if (a.charAt(indA) == b.charAt(indB)) {
        attempts[attempts.length-1] += a.charAt(indA);
        indA += 1;
      }

      // Increment a if b is limited
      indA += (indB == b.length - 1) ? 1 : 0;
      // Limit B if necessary
      indB += (indB < b.length - 1) ? 1 : 0;
    }
    start += 1;
  }
  
  let maxIndex = (arr) => arr.reduce((p, c) => (p.length < c.length) ? c : p, '');

  return maxIndex(attempts);
}
