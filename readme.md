The modules here represent algorithmic problems and their solutions.

Things to consider:
* Case sensitivity matters
* There are often multiple correct answers that your algorithm should account for
